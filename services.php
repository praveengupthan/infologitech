<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php'
     ?>     
      <!--/ styles -->
</head>
<body class="sub-body">

    <?php
        include'includes/header.php'
    ?>
    
    <div class="links">
        <a id="a1" data-id="a1" href="#1"><span>Service Intro</span></a>
        <a id="a2" data-id="a2" href="#2"><span>Enterprise Application Integration</span></a>
        <a id="a3" data-id="a3" href="#3"><span>ETL</span></a>
        <a id="a4" data-id="a4" href="#4"><span>Business Intelligence Analytics & Big Data</span></a>       
        <a id="a5" data-id="a5" href="#5"><span>Custom Application Development</span></a>      
        <a id="a6" data-id="a6" href="#6"><span>Quality Assurance</span></a>   
        <a id="a7" data-id="a7" href="#7"><span>Project Management</span></a>   
        <a id="a8" data-id="a8" href="#8"><span>Enterprise Resource Planning</span></a> 
        <a id="a9" data-id="a9" href="#9"><span>Content Management</span></a> 
        <a id="a10" data-id="a10" href="#10"><span>Infrastructure Management</span></a> 
    </div>
   
    <!--main-->
    <main class="subPage-Main domains-page">

        <!-- sub  page header -->
        <div class="subpage-header services-header" id="1">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- brudcrumb-->
                        <ul class="brcrumb nav wow animate__animated animate__fadeInUp">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>                               
                            </li>
                            <li>
                                <a class="nav-link">Services</a>                               
                            </li>
                        </ul>
                        <!--/ brudcrumb -->
                        <h1 class="text-left wow animate__animated animate__fadeInUp">Services</h1>
                        <p class="text-left wow animate__animated animate__fadeInUp">We Work on</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->  
            <a href="javascript:void(0)" class="move-top-video animate__animated animate__shakeY animate__infinite"><span class="icon-angle-double-down icomoon"></span></a>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body py-0">

        <!-- health care -->
        <section class="slidediv domainsdiv white-section" id="2">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/enterprise-application-development.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                       
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Enterprise Application Integration</h2>     
                         <p>Our integration experts provide highly scalable and high performing and readily adaptable solutions based on Clients future and current requirements.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ health care -->

         <!-- ETL -->
         <section class="services slidediv domainsdiv " id="3">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6 order-md-last wow animate__animated animate__fadeInUp">
                        <figure class="domainimg">
                            <img src="img/data-analytics.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                       
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">ETL </h2>     
                         <p>Extracting value from data to enhance products & services, either through systems integration, data analytics or strategic consulting, our team can support your data drive experience.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ ETL-->


        <!-- Business Intelligence Analytics & Big Data -->
        <section class="services slidediv domainsdiv white-section" id="4">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/bigdata.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                       
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Business Intelligence Analytics & Big Data </h2>     
                         <p>Organizations are applying innovative ways to derive competitive edge from their data artefacts. Our experts aid them to implement and support the complexity of Organizational Data into their Asset.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ NBusiness Intelligence Analytics & Big Data -->

         <!-- Custom Application Development -->
         <section class="services slidediv domainsdiv " id="5">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6 order-md-last">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/customapplication-development.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                       
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Custom Application Development</h2>     
                         <p>Enterprise Applications are at the core of every organization’s IT structure. We have the expertise to design and develop web applications using cutting edge technologies.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Custom Application Development -->

        <!-- Quality Assurance  -->
        <section class="services slidediv domainsdiv white-section" id="6">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/quality-assurance.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                      
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Quality Assurance </h2>     
                         <p>Our Quality Engineering & Assurance Associates provide reliable support across a range of hardware and software by testing applications, products to ensure high Quality and Reliability.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Quality Assurance -->

         <!-- Project Management -->
         <section class="services slidediv domainsdiv " id="7">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6 order-md-last">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/project-management.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                      
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Project Management </h2>     
                         <p>The success of any Business is derived from the fact that the Organization has the ability to put into action any great idea which enhances the Business.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Project Management -->

         <!-- Enterprise Resource Planning -->
         <section class="services slidediv domainsdiv white-section" id="8">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/erp.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                      
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Enterprise Resource Planning </h2>     
                         <p>Organizations share data across departments (manufacturing, purchasing, sales, accounting, Human Resources) using solutions provided by SAP, Oracle or Microsoft. Our Experts facilitate in information flow between all business functions, and manage connections to all stakeholders.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Enterprise Resource Planning-->

        <!-- Content Management -->
        <section class="services slidediv domainsdiv " id="9">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6 order-md-last">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/content-management.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                       
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Content Management </h2>     
                         <p>Our Services include but not limited to Web information, e-commerce, knowledge/workflow portals with full spectrum of content management solutions by Industry leaders. Our professionals have the expertise & experience to support, implement the industry leading content management suites at Organization level.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Content Management-->

          <!-- Infrastructure Management -->
          <section class="services slidediv domainsdiv white-section" id="10">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row row-service">
                    <!-- col -->
                    <div class="col-sm-6 order-md-last">
                        <figure class="domainimg wow animate__animated animate__fadeInUp">
                            <img src="img/it-infra.jpg" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-sm-6 align-self-center wow animate__animated animate__fadeInUp">                       
                         <h2 class="section-title fwhite w-100 pb-sm-4 pt-2 pt-sm-0 text-left">Infrastructure Management </h2>     
                         <p>The need of the hour is continuous change and upgrade to IT Infrastructure. Organizations are put into extreme pressure to operate at the speed ahead of business. Our Services enable them to be in ready and steady state, unlock the trapped value of their Infrastructure and support to deliver their critical Business success.</p>
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Infrastructure Management-->

        </div>
        <!--/ sub page body -->
    
    </main>
    <!--/ main -->

    <?php
        include'includes/footer.php'
    ?>
    <?php
        include'includes/scripts.php'
    ?>    
   
</body>
</html>