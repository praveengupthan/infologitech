<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php'
     ?>     
      <!--/ styles -->
</head>
<body class="sub-body">

    <?php
        include'includes/header.php'
    ?>   
   
   
    <!--main-->
    <main class="subPage-Main">

        <!-- sub  page header -->
        <div class="subpage-header domain-header">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- brudcrumb-->
                        <ul class="brcrumb nav wow animate__animated animate__fadeInUp">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>                               
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Domain</a>                               
                            </li>
                        </ul>
                        <!--/ brudcrumb -->
                        <h1 class="text-left  wow animate__animated animate__fadeInUp">Industry Expertise</h1>
                        <p class="text-left wow animate__animated animate__fadeInUp d-none d-sm-block">We as professionals work for these enterprises and provide various solutions that need to be tailored to the unique demands of target industry and its dynamics.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->  
            <a href="javascript:void(0)" class="move-top-video animate__animated animate__shakeY animate__infinite"><span class="icon-angle-double-down icomoon"></span></a>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body services-page py-0">
            <!-- container -->
            <div class="container">
                <!--row -->
                <div class="row domainrow" id="hCare">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center wow animate__animated animate__fadeInUp">
                       <img src="img/data/domains/health.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Health Care</h2>
                        <p class="wow animate__animated animate__fadeInUp">Our Consulting Services cater to Health Insurance service providers, Pharmacy chain management, Physician and Diagnostic Service Providers, Clearing House and Health Insurance Service beneficiaries. Integrating data from a wide range of clinical applications and making the data available upstream and downstream in the process flow is critical for achieving the business objectives of multiple parties in the Health care service industry. Our associates with their expertise enable the Organizations to provide comprehensive services and integrate Electronic Health Records (EHR) systems and to implement Health Information Exchanges (HIE).</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                <!--row -->
                <div class="row domainrow" id="Networking">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center order-md-last wow animate__animated animate__fadeInUp">
                        <img src="img/data/domains/networking.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center wow animate__animated animate__fadeInUp">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Networking &amp; Communications </h2>     
                         <p class="wow animate__animated animate__fadeInUp">Communication and Network companies have transformed themselves from being a basic telephony service to multiple-service providers such as broadband Internet access, television and telephone with wireless service provisions (quad play). They now face increasing competition and need to excel with higher speeds and better / consistent connectivity. Our team of business and technology consultants are expert in the latest IT platforms including the Cloud, Mobility can help the companies reap benefits by providing integrated solutions.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                
                <!--row -->
                <div class="row domainrow" id="Insurance">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center wow animate__animated animate__fadeInUp">
                        <img src="img/data/domains/insurance.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                     <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Insurance </h2>     
                         <p class="wow animate__animated animate__fadeInUp">We have experts that specialize in platforms specific to the insurance industry who can provide services on agent compensation, claims processing and policy administration. Our team of technology experts will help in Organization’s plans to modernize operations and attain best-in-class technology architecture while managing costs.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                <!--row -->
                <div class="row domainrow" id="Banking">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center order-md-last wow animate__animated animate__fadeInUp">
                    <img src="img/data/domains/banking.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Banking & Financial </h2>     
                         <p class="wow animate__animated animate__fadeInUp">Our consulting services can support and enhance the capabilities to execute the day-to-day business better and efficiently. The domain expertise of our associates can be utilized to provide business solutions with varied spectrum of banking and financial services.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                <!--row -->
                <div class="row domainrow" id="#Logistics">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center wow animate__animated animate__fadeInUp">
                     <img src="img/data/domains/logistics.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Logistics </h2>     
                         <p class="wow animate__animated animate__fadeInUp">From carrier management, rate negotiation, freight bill audit and payment, routing compliance, and shipment execution and tracking, higher realization through automated order to cash flow, Our associates have worked with the leading transportation & Logistics Companies and helped with providing their expertise in building end-to-end solutions.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                 <!--row -->
                 <div class="row domainrow" id="Education">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center order-md-last wow animate__animated animate__fadeInUp">
                        <img src="img/data/domains/studying.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Education </h2>     
                         <p class="wow animate__animated animate__fadeInUp">Redefining student experience to managing connectivity between students with colleges and universities, working with testing and assessment providers, integrating various internal systems with trading partners, our associates have been working with passion to in helping the Clients provide the best services.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                <!--row -->
                <div class="row domainrow" id="Manufacturing">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center wow animate__animated animate__fadeInUp">
                        <img src="img/data/domains/manufacturing.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Manufacturing </h2>     
                        <p class="wow animate__animated animate__fadeInUp">Our highly talented Business consulting associates can deliver services to the manufacturing sector clientele to ensure that they meet return on technology investments goals and reaching the business targets.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                  <!--row -->
                  <div class="row domainrow" id="Retail">
                    <!-- col -->
                    <div class="col-md-6 text-center align-self-center order-md-last wow animate__animated animate__fadeInUp">
                        <img src="img/data/domains/retail.svg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                     <!-- col -->
                     <div class="col-md-6 align-self-center">
                        <h2 class="section-title pb-3 text-left wow animate__animated animate__fadeInUp">Retail </h2>     
                        <p class="wow animate__animated animate__fadeInUp">With the increasing globalized shopping experiences available through the Web, it has become crucial for specialty retailers to work smarter in order to stay ahead in their Business. Infologitech has the expertise to help retailers capitalize on their niche and continue to remain on the top with the competitive edge. The associates with their vast experience can help retailers identify the bottlenecks and create new business opportunities with technology.</p>
                     </div>
                    <!--/col -->
                </div>
                <!--/ row -->

            </div>
            <!--/ container -->
       
        </div>
        <!--/ sub page body -->
    
    </main>
    <!--/ main -->

    <?php
        include'includes/footer.php'
    ?>
    <?php
        include'includes/scripts.php'
    ?>    
   
</body>
</html>