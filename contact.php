<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php'
     ?>     
      <!--/ styles -->
</head>
<body class="sub-body">

    <?php
        include'includes/header.php'
    ?>   
    <?php
        include 'includes/arrayObjects.php';
    ?>
   
   
    <!--main-->
    <main class="subPage-Main">

        <!-- sub  page header -->
        <div class="subpage-header contact-header">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- brudcrumb-->
                        <ul class="brcrumb nav wow animate__animated animate__fadeInUp">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>                               
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Contact</a>                               
                            </li>
                        </ul>
                        <!--/ brudcrumb -->
                        <h1 class="text-left wow animate__animated animate__fadeInUp">How can we help you?</h1>                                              
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->  
            <a href="javascript:void(0)" class="move-top-video animate__animated animate__shakeY animate__infinite"><span class="icon-angle-double-down icomoon"></span></a>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <div class="subpage-body">
             <!-- container -->
             <div class="container">
                <div class="row py-5">
                    <!-- col -->
                    <div class="col-lg-4 col-sm-6 col-md-6 contact-col text-center wow animate__animated animate__fadeInUp" >
                        <h5 class="h5 pb-3">Email</h5>
                        <div class="icon-col">
                            <a href="mailto:operations@infologitech.com" target="_blank"><span class="icon-mail-2 icomoon"></span></a>
                        </div>
                        <p class="text-center">
                            <a href="mailto:operations@infologitech.com">operations@infologitech.com</a>
                        </p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-6 col-md-6 contact-col text-center wow animate__animated animate__fadeInUp" >
                        <h5 class="h5 pb-3">Call us</h5>
                        <div class="icon-col">
                        <a href="tel:+17329272460"> <span class="icon-phone icomoon"></span></a>
                        </div>
                        <p class="text-center mb-0">+1 732 927 2460</p>                       
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-12 col-md-6 contact-col text-center wow animate__animated animate__fadeInUp">
                        <h5 class="h5 pb-3">Career</h5>
                        <div class="icon-col">
                            <a href="mailto:info@infologitech.com" target="_blank">
                                <span class="icon-research icomoon"></span>
                            </a>
                        </div>
                        <p class="text-center mb-0">
                            <a href="mailto:support@prewellabs.com">info@infologitech.com</a>
                        </p>                       
                    </div>
                    <!--/ col -->                        
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row no-gutters">
                    <!-- col -->
                    <div class="col-md-4 contactcol wow animate__animated animate__fadeInUp">
                        <article>
                            <h4 class="h3">New Jersey, USA</h4>
                            <p>3, Independence Way, Suite 117, Princeton, NJ 08540</p>
                        </article>
                        <img src="img/contact01.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ co -->
                    <!-- col -->
                      <div class="col-md-4 contactcol wow animate__animated animate__fadeInUp">
                        <article>
                            <h4 class="h3">Michigan, USA</h4>
                            <p>41000, Woodward Avenue, Suite 350, Bloomfield Hills, MI 48304</p>
                        </article>
                        <img src="img/contact02.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ co -->
                     <!-- col -->
                     <div class="col-md-4 contactcol wow animate__animated animate__fadeInUp">
                        <article>
                            <h4 class="h3">Hyderabad, India</h4>
                            <p>B 1002, The Platina, Gachibowli, Hyderabad -500032</p>
                        </article>
                        <img src="img/contact03.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ co -->
                </div>
                <!--/ row -->
             </div>
             <!--/ container -->
        </div>
        <!--/ sub page body -->

    </main>
    <!--/ main -->   

    <?php
        include'includes/footer.php'
    ?>
    
    <?php
        include'includes/scripts.php'
    ?>    
   
</body>
</html>