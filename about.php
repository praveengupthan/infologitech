<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php'
     ?>     
      <!--/ styles -->
</head>
<body class="sub-body">

    <?php
        include'includes/header.php'
    ?>
   
    <!--main-->
    <main class="subPage-Main">

        <!-- sub  page header -->
        <div class="subpage-header about-header ">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- brudcrumb-->
                        <ul class="brcrumb nav wow animate__animated animate__fadeInUp">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>                               
                            </li>
                            <li>
                                <a class="nav-link">About</a>                               
                            </li>
                        </ul>
                        <!--/ brudcrumb -->
                        <h1 class="wow animate__animated animate__fadeInUp">About Infologitech</h1>                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->  
            <a href="javascript:void(0)" class="move-top-video animate__animated animate__shakeY animate__infinite "><span class="icon-angle-double-down icomoon"></span></a>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body py-0">
            <!-- overview -->
            <div class="overview-about" id="overview">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col-->
                        <div class="col-md-6 align-self-center">
                            <h2 class="h1 fbold wow animate__animated animate__fadeInUp">Who We are </h2>
                            <h5 class="flight h4 wow animate__animated animate__fadeInUp">Founded in 2006, InfoLogitech is a fast growing medium sized IT development and services organization. The company has been nourishing a high growth rate YOY.</h5>

                            <!-- row -->
                            <div class="row py-5">
                                <!-- col -->
                                <div class="col-sm-6 col-6 countcol wow animate__animated animate__fadeInUp">
                                    <h2 class="h1 fbold">10</h2>
                                    <p class="text-left">Years of Experience</p>
                                </div>
                                <!--/col -->
                                 <!-- col -->
                                 <div class="col-sm-6 col-6 countcol wow animate__animated animate__fadeInUp">
                                    <h2 class="h1 fbold">100+</h2>
                                    <p class="text-left">Happy Clients</p>
                                </div>
                                <!--/col -->
                            </div>
                            <!--/ row -->
                        </div>
                        <!-- /col-->
                        <!-- col-->
                        <div class="col-md-6 wow animate__animated animate__fadeInUp">
                            <p> InfoLogitech has its headquarters in Princeton, New Jersey, with an operating office in Bloomfield Hills, Michigan and an international office in Hyderabad, India.</p>
                            <p>INFOLOGITECH INC is a global IT development and Services company delivering reliable, efficient, and profitable solutions to our clients through the four I's of business technology: Integration, Implementation, Infrastructure, and Innovation. We provide expertise in the area of Digital Transformation to Organizations and help them Plan, Build and Transform their Businesses to the next level. InfoLogitech has an experienced, agile and highly motivated team of professionals with a dedicated endeavour to address the Systems Engineering, Enterprise Application Integration/Automation and Digital Transformation needs of our clients.</p>
                            <p>Infologitech is proud to be associated with Microsoft as most highly accredited independent technical support providers as part of Microsoft Partner network. We are Microsoft Gold certified Partner.</p>
                        </div>
                        <!-- /col-->
                    </div>
                    <!--/row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ over view -->

            <!-- core values -->
            <div class="core-values sectionpad lightgreenbg" id="coreValues">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-12 text-center py-sm-4 py-1">
                            <h2 class="h2 sectiontitle fbold pb-2 pb-sm-5 wow animate__animated animate__fadeInUp">Our Core Values</h2>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-sm-4 valuescol wow animate__animated animate__fadeInUp">
                            <img src="img/client-satisfaction.jpg" alt="" class="img-fluid d-none d-sm-block">
                            <article>
                                <h3 class="h4 py-2 mb-0">Client Satisfaction</h3>
                                <p class="text-left">Focus on providing a superior customer experience each and every time thru service excellence and play a role in client’s success.</p>
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-sm-4 valuescol border-left border-right wow animate__animated animate__fadeInUp">                          
                            <article>
                                <h3 class="h4 py-2 mb-0">Above and Beyond</h3>
                                <p class="text-left">We strive to go above and beyond the expectations of our clients (and our employees) and work towards Value creation.</p>
                            </article>
                            <img src="img/abovebeyond.jpg" alt="" class="img-fluid d-none d-sm-block">
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-sm-4  valuescol wow animate__animated animate__fadeInUp">
                            <img src="img/integrity.jpg" alt="" class="img-fluid d-none d-sm-block">
                            <articler>
                                <h3 class="h4 py-2 mb-0">Integrity</h3>
                                <p class="text-left">Being honest and having strong moral principles; moral uprightness. Conduct Business with consistently moral and ethical standards and enclosed with people who care deeply about integrity.</p>
                            </articler>
                        </div>
                        <!--/ col -->
                        
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ core values -->

            <!-- vision and missiong -->
            <div class="visionMission sectionpad ligtgraybg" id="visionMission">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-6 wow animate__animated animate__fadeInUp ">
                            <h2 class="h1 sectiontitle fbold">Our Vision</h2>
                            <p>To create a better technology environment for the Organizations by offering a wide range of highly skilled affordable software services Mission.</p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6 wow animate__animated animate__fadeInUp">
                        <h2 class="h1 sectiontitle fbold">Our Mission</h2>
                            <p>To provide superior and competent software services that make Organizations perform better in their Business and create value to their customers.</p>
                        </div>
                        <!--/ col -->
                       
                    </div>
                    <!--/ row -->                    
                </div>
                <!---/ container -->
            </div>
            <!--/ vision and mission -->

            <!-- team -->
            <div class="team sectionpad" id="leader">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row teamrow">
                        <div class="col-md-12 wow animate__animated animate__fadeInUp">
                            <h2 class="h2 sectiontitle fbold text-sm-right">Our Leaders</h2>                          
                        </div>
                        <!-- col -->
                        <div class="col-md-3 teamimg align-self-center wow animate__animated animate__fadeInUp">
                            <img src="img/srini.jpg" alt="">
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-9 wow animate__animated animate__fadeInUp">
                             <h5 class="fbold mb-0">Srinivas Makkuri</h5>
                             <p class="fsbold fgray">President &amp; CEO</p>
                             <p>Srini Makkuri serves as President and CEO of InfoLogitech Inc., providing vision, strategy and direction to the organization. Mr. Makkuri has built the organization over a decade with employee satisfaction and customer loyalty as primary guiding principles. A technology leader with over two decades of experience in Information Technology has served organizations in Health Care, Financial, Manufacturing and Insurance domains. Prior to InfoLogitech Mr. Makkuri served at Compuware, Ford, Wipro and Satyam (now Tech Mahindra) in developing technology solutions. Mr. Makkuri earned a bachelor’s degree in Electronics and Communications Engineering from Osmania University in India </p>
                         </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row teamrow">                      
                        <!-- col -->
                        <div class="col-md-3 teamimg align-self-center order-md-last wow animate__animated animate__fadeInUp">
                            <img src="img/hari.jpg" alt="">
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-9 wow animate__animated animate__fadeInUp">
                             <h5 class="fbold mb-0 text-md-right">Hari Krishna Gurajala</h5>
                             <p class="fsbold fgray text-md-right">Sr. Vice President</p>
                             <p>Having around 2 decades of experience with Product and Services Organizations. Implemented strategies and policies focused for improving overall customer experience. Is a Senior Vice-President taking care of Client and Employee relations of the Company. Before joining InfoLogitech, was associated with Oracle, ADP and CMC / TCS. Fulfilling key roles in driving significant growth to customers, employees and revenue, while leading various Development and Support Teams. Worked on high visibility Domestic and International Projects and delivered Quality and Value-added services to Customers. Clients in India include Indian Railways, General Insurance, Government of Karnataka and International Clients include American Red Cross, Pittsburgh Police, Noble Energy and ThyssenKrupp. Holds a Bachelors of Engineering degree from Osmania University, ITIL certified. </p>
                         </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                      <!-- row -->
                      <div class="row teamrow">                      
                        <!-- col -->
                        <div class="col-md-3 teamimg align-self-center wow animate__animated animate__fadeInUp">
                            <img src="img/Henry.jpg" alt="">
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-9 align-self-center wow animate__animated animate__fadeInUp">
                             <h5 class="fbold mb-0">Henry Wood</h5>
                             <p class="fsbold fgray">Sr. Vice President</p>
                             <p>Henry is focused on Business Development leading all client relationship experiences. Henry has over 20 years’ experience in the IT industry and still claims he’s a developer. Henry earned his Bachelor’s of Science MIS degree from the University of Iowa and his MBA from Drake University. </p>
                         </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row teamrow">                      
                        <!-- col -->
                        <div class="col-md-3 teamimg align-self-center order-md-last wow animate__animated animate__fadeInUp">
                            <img src="img/mohan.jpg" alt="">
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-9 wow animate__animated animate__fadeInUp">
                             <h5 class="fbold mb-0 text-md-right">Mohan Midatana</h5>
                             <p class="fsbold fgray text-md-right">Vice President</p>
                             <p>As vice president of IT services, Mohan is committed to helping the organization to develop and drive the business forward. Mohan brings with him more than 20 years of experience in building and growing organizations of many fortune 500 companies primarily focused on leading large program teams, product implementations across US, EMEA and Asia Pacific regions. Mohan received Master’s in computer applications from Andhra University, India.</p>
                         </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row teamrow">                      
                        <!-- col -->
                        <div class="col-md-3 teamimg align-self-center wow animate__animated animate__fadeInUp">
                            <img src="img/munish.jpg" alt="">
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-9 align-self-center wow animate__animated animate__fadeInUp">
                             <h5 class="fbold mb-0">Munish Kumar</h5>
                             <p class="fsbold fgray">Vice President</p>
                             <p>Mr. Munish Kumar as Vice President takes care of delivery of Application Software Services. Accomplished technology leader with over 20 years of experience in formulating and implementing strategies, managing highly complex information technology projects across the industry. Prior to InfoLogitech Mr. Kumar served at First Group America, IBM and Infosys. Mr. Kumar earned a bachelor of technology degree from IIT-Kanpur, India. </p>
                         </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->




                </div>
                <!--/ container -->
            </div>
            <!--/ team -->
        </div>
        <!--/ sub page body -->
    
    </main>
    <!--/ main -->

    <?php
        include'includes/footer.php'
    ?>
    <?php
        include'includes/scripts.php'
    ?>    
   
</body>
</html>