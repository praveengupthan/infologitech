<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php'
     ?>     
      <!--/ styles -->
</head>
<body class="sub-body">

    <?php
        include'includes/header.php'
    ?>   
    <?php
        include 'includes/arrayObjects.php';
    ?>
   
   
    <!--main-->
    <main class="subPage-Main">

        <!-- sub  page header -->
        <div class="subpage-header clients-header">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- brudcrumb-->
                        <ul class="brcrumb nav wow animate__animated animate__fadeInUp">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>                               
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Clients</a>                               
                            </li>
                        </ul>
                        <!--/ brudcrumb -->
                        <h1 class="text-left wow animate__animated animate__fadeInUp">Clients</h1>                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->  
            <a href="javascript:void(0)" class="move-top-video animate__animated animate__shakeY animate__infinite"><span class="icon-angle-double-down icomoon"></span></a>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">

           <!-- tab -->
           <div class="parentVerticalTab pt-sm-5">
                <ul class="resp-tabs-list hor_1 wow animate__animated animate__fadeInUp">
                    <li>Health Care</li>
                    <li>Banking & Finance</li>
                    <li>Manufacturing</li>
                    <li>Insurance</li>
                    <li>Software</li>
                    <li>Networking & Communications</li>
                    <li>Transport & Logistics</li>
                    <li>Education & other Services</li>
                    <li>Media & Entertainment</li>
                    <li>Federal & State</li>
                </ul>
                <div class="resp-tabs-container hor_1 wow animate__animated animate__fadeInUp">
                     <!--Health Care -->
                    <div>                        
                        <!-- row -->
                        <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($healthClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $healthClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->                       
                    </div>
                     <!--/Health Care -->
                     <!--Banking & Finance -->
                     <div>                        
                       <!-- row -->
                       <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($bankingClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $bankingClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->         
                    </div>
                     <!--/Banking & Finance -->
                      <!--Manufacturing -->
                      <div>                        
                       <!-- row -->
                       <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($ManufacturingClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $ManufacturingClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                     <!--/Manufacturing -->
                      <!--Insurance -->
                      <div>                        
                       <!-- row -->
                       <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($insuranceClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $insuranceClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                     <!--/Insurance -->
                      <!--Software -->
                      <div>                        
                       <!-- row -->
                       <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($softwareClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $softwareClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                     <!--/Software -->
                      <!--Networking & Communications -->
                      <div>                        
                        <!-- row -->
                        <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($networkingClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $networkingClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                     <!--/Networking & Communications -->
                    <!--Transport & Logistics -->
                    <div>                        
                        <!-- row -->
                        <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($transportClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $transportClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/Transport & Logistics -->
                     <!--Education & other Serices -->
                     <div>                        
                        <!-- row -->
                        <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($educationClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $educationClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/Education & other Serices -->
                      <!--Media & Entertainment -->
                      <div>                        
                        <!-- row -->
                        <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($mediaClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $mediaClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/Media & Entertainment -->
                     <!--Federal & State -->
                     <div>                        
                        <!-- row -->
                        <div class="row clientsrow">
                            <?php
                            for($i=0;$i<count($federalClients);$i++) {?>
                                <div class="col-sm-4 col-6">
                                    <img src="img/clients/<?php echo $federalClients[$i][0]?>" alt="" class="img-fluid">
                                </div>                        
                            <?php } ?>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/Federal & State -->
                </div>
            </div>
           <!--/ tab -->

           </div>
           <!--/container -->       
        </div>
        <!--/ sub page body -->
    
    </main>
    <!--/ main -->

    <?php
        include'includes/footer.php'
    ?>
    <?php
        include'includes/scripts.php'
    ?>    
   
</body>
</html>