<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php'
     ?>     
      <!--/ styles -->
</head>
<body class="sub-body">

    <?php
        include'includes/header.php'
    ?>   
    <?php
        include 'includes/arrayObjects.php';
    ?>
   
   
    <!--main-->
    <main class="subPage-Main">

        <!-- sub  page header -->
        <div class="subpage-header careers-header">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- brudcrumb-->
                        <ul class="brcrumb nav wow animate__animated animate__fadeInUp">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>                               
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Careers</a>                               
                            </li>
                        </ul>
                        <!--/ brudcrumb -->
                        <h1 class="text-left wow animate__animated animate__fadeInUp">Careers at Infologitech</h1>                                              
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->  
            <a href="javascript:void(0)" class="move-top-video animate__animated animate__shakeY animate__infinite"><span class="icon-angle-double-down icomoon"></span></a>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">

           <h2 class="section-title  pb-2 text-center wow animate__animated animate__fadeInUp">Join Our Team </h2>     
           <p class="text-center wow animate__animated animate__fadeInUp">InfoLogitech provides a stable framework for success from multiple dimensions. We encourage and sponsor training for retooling of skills, provide mentoring and offer reimbursement for certifications. You will have access to people and resources that help you design and build your career path – with the benefit of the real-life experiences of someone who has been there before you.
            </p>

            <h2 class="section-title  py-2 text-center wow animate__animated animate__fadeInUp">What We Offer </h2>  
            <!-- career row -->
            <div class="row pb-3">
                <!-- col -->
                <div class="col-md-6 careercol wow animate__animated animate__fadeInUp">
                    <h4 class="h4">Health Insurance</h4>
                    <p>Comprehensive Health, Life and disability Insurance for the family</p>
                    <span class="icon-health icomoon"></span>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 careercol wow animate__animated animate__fadeInUp">
                    <h4 class="h4">Retirement Savings</h4>
                    <p>Option to enroll on 401K Retirement Plans.</p>
                    <span class="icon-pension icomoon"></span>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 careercol wow animate__animated animate__fadeInUp">
                    <h4 class="h4">Paid Vacation Leave Per Year</h4>
                    <p>Paid vacation leave per year, paid holidays, personal leave</p>
                    <span class="icon-beach icomoon"></span>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 careercol wow animate__animated animate__fadeInUp">
                    <h4 class="h4">Employee Training and Professional Certification</h4>
                    <p>Reimbursement of costs towards skill upgrade and professional certification</p>
                    <span class="icon-award icomoon"></span>
                </div>
                <!--/ col -->
            </div>
            <!-- /career row -->

            <h2 class="section-title  py-3 text-center wow animate__animated animate__fadeInUp">Active Job Postings </h2>  

            <div class="current-jobpostings wow animate__animated animate__fadeInUp">

            <!-- accordion-->
           <div class="accordion">
                <h3 class="panel-title">Job Position: Software Engineer(s):</h3>
                <div class="panel-content">
                    <ul class="list-items">
                        <li>INFOLOGITECH, INC. in Princeton, NJ seeks Software Engineers to design, develop, implement and test various software applications such as Enterprise Applications, Application Integration, Data Processing Applications utilizing knowledge of technologies like .Net Framework or Java Framework or BizTalk or SQLServer that enable business/data processing or service layer or data mapping solutions.</li>
                        <li>Frequent travel required to unanticipated client sites in the U.S.A.
                        </li>
                        <li> Requires Master’s Degree (or educ. equiv.) in Engineering or Science or Math or Business Administration or Computer Information Systems or related, and Alternatively, will accept Bachelor’s (or educ. equiv.) Degree in Engineering or Science or Math or Business Administration or Computer Information Systems or related, and five (5) yrs. experience in the job offered or related occupation.</li>
                        <li>Date Posted: 04/28/2020</li>
                        <li>Resume to: INFOLOGITECH, INC., Attn: HR Department, 3 Independence Way, Suite 117, Princeton, NJ, 08540</li>
                    </ul>
                    <p class="pt-4">
                        <a data-toggle="modal" data-target="#applyJob" href="javascript:void(0)" class="brd-link custlink">Apply Now</a>
                    </p>
                </div>
                <h3 class="panel-title">Job Position: Software Engineer(s):</h3>
                <div class="panel-content">
                    <ul class="list-items">
                        <li>INFOLOGITECH, INC. in Princeton, NJ seeks Software Engineers to design, develop, implement and test various software applications such as Enterprise/Big Data Analytics, Integration, Data Processing Applications utilizing knowledge of technologies like BizTalk or SQLServer or Java or .Net and software frameworks that enable data analytics or map-reduce or data mapping solutions.</li>
                        <li>Frequent travel required to unanticipated client sites in the U.S.A.
                        </li>
                        <li> Requires Master’s Degree (or educ. equiv.) in Engineering or Science or Math or Business Administration or Computer Information Systems or related, and Alternatively, will accept Bachelor’s (or educ. equiv.) Degree in Engineering or Science or Math or Business Administration or Computer Information Systems or related, and five (5) yrs. experience in the job offered or related occupation.</li>
                        <li>Date Posted: 01/03/2020</li>
                        <li>Resume to: INFOLOGITECH, INC., Attn: HR Department, 3 Independence Way, Suite 117, Princeton, NJ, 08540</li>
                    </ul>
                </div>

                <h3 class="panel-title">Job Position: Software Engineer:</h3>
                <div class="panel-content">
                    <ul class="list-items">
                        <li>INFOLOGITECH, INC. in Princeton, NJ seeks Software Engineers to design, develop, implement &amp; test various Enterprise Application and Integration solutions utilizing knowledge of technologies like .Net or JAVA/J2EE or BizTalk or WMB.</li>
                        <li>Frequent travel required to unanticipated client sites in the U.S.A.
                        </li>
                        <li> Bachelor’s (or educ. Equiv.) Degree in Engineering or Science or Math or Business Administration or Computer Information Systems or related and Five(5) years of(post-degree, progressive) experience in job offered or related occupation in lieu of Master’s Degree.</li>
                        <li>Date Posted: 03/05/2018</li>
                        <li>Resume to: Resume to Job Location: InfoLogitech, Inc., Attn: HR, 3 Independence way, Ste. 403, Princeton, NJ 08540</li>
                    </ul>
                </div>

                <h3 class="panel-title">Job Position: Software Engineers:</h3>
                <div class="panel-content">
                    <ul class="list-items">
                        <li>INFOLOGITECH, INC. in Princeton, NJ seeks Software Engineers to design, develop, implement and test various Enterprise Data Analytics Applications, integration and data-warehousing solutions utilizing knowledge of technologies like BizTalk or SQL Server or Informatica or .Net.</li>
                        <li>Requires Master’s Degree (or educ. equiv.) in Engineering or Science or Math or Business Administration or Computer Information Systems or related, and Alternatively, will accept Bachelor’s (or educ. equiv.) Degree in Engineering or Science or Math or Business Administration or Computer Information Systems or related, and five (5) yrs. experience in the job offered or related occupation. Frequent travel required to unanticipated client sites in USA.</li>
                        <li>Date Posted: 01/11/2018</li>
                        <li>Resume to: INFOLOGITECH, INC., Attn: HR Department, 3 Independence Way, Suite 403, Princeton, NJ, 08540</li>
                    </ul>
                </div>

                <h3 class="panel-title">Job Position: Programmer Analyst:</h3>
                <div class="panel-content">
                    <ul class="list-items">
                        <li>INFOLOGITECH, INC. in Princeton, NJ seeks Design, develop, enhance, integrate and implement applications and systems based on business requirements utilizing knowledge of BizTalk Server, C#, .Net, SQL Server, Oracle, XML.</li>
                        <li>Requires Bachelor's degree in Math, Science, Engineering or Business Administration Discipline (or foreign equivalent degree) OR Bachelor's Degree in any field and two years' work experience in the position offered or as a Software Engineer or in a related job role. Frequent travel required to unanticipated client sites in USA. </li>
                        <li>Date Posted: 08/22/2017</li>
                        <li>Resume to: INFOLOGITECH, INC., Attn: HR Department, 3 Independence Way, Suite 403, Princeton, NJ, 08540</li>
                    </ul>
                </div>               
            </div>
            <!-- accordion-->
                
        </div>

           

           </div>
           <!--/container -->       
        </div>
        <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    

    <?php
        include'includes/footer.php'
    ?>
    <?php
        include'includes/scripts.php'
    ?>    

    <!-- job application -->
    <!-- Modal -->
<div class="modal fade" id="applyJob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Job Title Job Title </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form class="form">
           <!-- row -->
           <div class="row">
               <!-- col -->
               <div class="col-md-12">
                   <div class="form-group">
                       <label>First Name</label>
                       <div class="input-group">
                           <input type="text" class="form-control" placeholder="Write First Name">
                       </div>
                   </div>
               </div>
               <!--/ col -->
                <!-- col -->
                <div class="col-md-12">
                   <div class="form-group">
                       <label>Last Name</label>
                       <div class="input-group">
                           <input type="text" class="form-control" placeholder="Write Last Name">
                       </div>
                   </div>
               </div>
               <!--/ col -->
                <!-- col -->
                <div class="col-md-12">
                   <div class="form-group">
                       <label>Email</label>
                       <div class="input-group">
                           <input type="text" class="form-control" placeholder="Email">
                       </div>
                   </div>
               </div>
               <!--/ col -->
                <!-- col -->
                <div class="col-md-12">
                   <div class="form-group">
                       <label>Phone Number</label>
                       <div class="input-group">
                           <input type="text" class="form-control" placeholder="Phone Number">
                       </div>
                   </div>
               </div>
               <!--/ col -->
                <!-- col -->
                <div class="col-md-12">
                   <div class="form-group">
                       <label>Attach Your Resume </label>
                       <div class="input-group">
                           <input type="file" class="form-control" placeholder="Attach Resume">
                       </div>
                   </div>
               </div>
               <!--/ col -->
           </div>
           <!--/ row -->
       </form>
      </div>
      <div class="modal-footer">       
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
    <!--/ job appliation -->
   
</body>
</html>