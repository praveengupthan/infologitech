 <!-- scripts -->
 <script src="js/jquery-3.5.1.min.js"></script>   
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
<script src="js/jquery.smoothScroll.js"></script> 
<script>
$( 'a[href^="#"]' ).SmoothScroll( {
        duration: 1000,
        easing  : 'easeOutQuint'
        } );	
</script>       
<script src="js/bootstrap.min.js"></script>

<script src="js/swiper.min.js"></script>

<!--[if lt IE 9]>
<script src="js/html5-shiv.js"></script>
<![end if ]-->    
<script src="js/bsnav.min.js"></script> 
<!-- animate wowjs-->
<script src="js/wow.min.js"></script>
<script src="js/easyResponsiveTabs.js"></script>    
<script src="js/accordion.js"></script>
<script src="js/custom.js"></script>