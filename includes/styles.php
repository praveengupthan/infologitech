<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<!--styles -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bsnav.min.css">
<link rel="stylesheet" href="css/animate-4.0.css">
<link rel="stylesheet" href="css/swiper.min.css">
<link rel="stylesheet" href="css/style.css">    
<link rel="stylesheet" href="css/easy-responsive-tabs.css"> 
<link rel="stylesheet" href="css/icomoon.css">