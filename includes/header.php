 
 <?php 
   include'includes/arrayObjects.php';
 ?>
 <!-- slide menu -->
 <div class="slide-nav row no-gutters justify-content-end" id="slideNav">
        <!-- left show sub menu  -->
        <div class="col-sm-6 col-md-6 col-lg-6 col-12 left-submenu" id="leftpopnav">

            <a hef="javascript:void(0)" class="closenav" id="closeNavmob">
                <span class="icon-close icomoon"></span>
            </a>
            
            <!--About -->
            <div class="submenu slideNavChild" id="navChild1">
                <!-- row -->
                <div class="row no-gutters">
                    <!--col -->
                    <div class="col-md-6">
                        <a href="about.php#overview" class="childdiv sub01_01">
                            <span class="icon-cup icomoon d-none d-sm-block"></span>
                            <h1 class="h3 fwhite p-4">
                               Who We are
                            </h1>
                        </a>
                       
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6">
                        <a class="colorcd1" href="about.php#coreValues">
                            <span class="icon-handshake-o icomoon d-none d-sm-block"></span>
                            <h1 class="h3 fwhite p-4">Core Values</h1>
                        </a>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-6">
                         <a class="colorcd2" href="about.php#visionMission">
                            <span class="icon-strategy icomoon d-none d-sm-block"></span>
                            <h1 class="h3 fwhite p-4">Vision &amp; Mission</h1>
                        </a>
                    </div>
                    <!--/ col -->
                      <!-- col -->
                      <div class="col-md-6 colorcd3">
                      <a class="colorcd3" href="about.php#leader">
                            <span class="icon-leadership icomoon d-none d-sm-block"></span>
                            <h1 class="h3  p-4">Leaders</h1>
                      </a>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ About-->

            <!-- Services -->
            <div class="submenu slideNavChild Industries" id="navChild2">
                <h3 class="py-4 Industries">Services</h3>
               <!-- row -->
               <div class="row">
                    <!-- col -->
                    <?php 
                    for($i=0;$i<count($serviceSubMenuItem);$i++) {?>
                    <div class="col-md-6">
                        <h5 class="h5">
                        <a href="services.php#<?php echo $serviceSubMenuItem[$i][0]?>"><span class="<?php echo $serviceSubMenuItem[$i][2]?> icomoon"></span><?php echo $serviceSubMenuItem[$i][1]?></a></h5>
                    </div>
                    <?php } ?>
                    <!--/ col -->                                
                </div>
                <!--/ row -->
            </div>
            <!--/ Services -->

            
            <!-- Domains -->
            <div class="submenu slideNavChild Industries" id="navChild3">
                <h3 class="py-4 Industries">Domains</h3>
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <?php 
                    for($i=0;$i<count($domainsSubmenuItems);$i++) {?>
                    <div class="col-md-6">
                        <h5 class="h5"><a href="domains.php#<?php echo $domainsSubmenuItems[$i][0]?>"><span class="<?php echo $domainsSubmenuItems[$i][2]?> icomoon"></span><?php echo $domainsSubmenuItems[$i][1]?></a></h5>
                    </div>
                    <?php } ?>
                    <!--/ col -->                                          
                </div>
                <!--/ row -->
            </div>
            <!--/ Domains -->

             
           
        </div>
        <!--/ left show sub menu -->
        <!-- right nav -->
        <div class="right-nav col-sm-6 col-md-4 col-lg-3 col-12">
            <a hef="javascript:void(0)" class="closenav" id="closeNav">
                <span class="icon-close icomoon"></span>
            </a>
            <a href="javascript:void(0)" class="submenu-brand">
                <img src="img/colourlogo.svg" alt="">
            </a>
            <ul class="primary-nav">
                <li>
                    <a href="index.php" class="navNextParent">Home</a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="navNextParent " data-target="#navChild1">About</a>
                    <!-- <a href="about.php" class="navNextParent d-block d-md-none">About</a> -->
                </li>
                <li>
                    <a href="javascript:void(0)" class="navNextParent " data-target="#navChild2" >Services</a>
                    <!-- <a href="services.php" class="navNextParent d-block d-md-none">Services</a> -->
                </li>
                <li>
                    <a href="javascript:void(0)" class="navNextParent " data-target="#navChild3">Domains</a>
                    <!-- <a href="domains.php" class="navNextParent d-block d-md-none">Domains</a> -->
                </li>            
               
            </ul>
            <ul class="secondary-nav">  
                <li><a href="clients.php">Clients</a></li>
                <li><a href="careers.php">Careers</a></li>
                <li><a href="contact.php">Contact</a></li>                
            </ul>
            <ul class="header-social border-top nav">
                <li class="nav-item">
                    <a class="nav-link" href="https://www.facebook.com/InfoLogitech.inc"><span class="icon-facebook icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/checkpoint/challengesV2/AQG8Mhe7PyiP7AAAAXP3vDG5dym4Rr1xHbTGssQ0BGFyB7WtNOxP-2kBMlP5d6jdMzqx0MR80SGMmnT0YYLsjXywPo-STcuVeQ"><span class="icon-linkedin icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://twitter.com/InfoLogitechILT"><span class="icon-twitter icomoon"></span></a>
                </li>
            </ul>
        </div>
        <!--/ right nav -->
    </div>
    <!--/ slide menu-->

	<!-- header -->
	<header class="home-header bsnav-sticky">
		<div class="navbar container d-flex justify-content-between">
        
            <a class="navbar-brand" href="index.php">
                <img src="img/colourlogo.svg" alt="">
            </a>		
		
            <ul class="nav mx-auto mid-nav">
                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="about.php">About</a></li>
                <li class="nav-item"><a class="nav-link" href="domains.php">Domain</a></li>
                <li class="nav-item"><a class="nav-link" href="services.php">Services</a></li>
                <li class="nav-item"><a class="nav-link" href="clients.php">Clients</a></li>
                <li class="nav-item"><a class="nav-link" href="careers.php">Careers</a></li>  
                <li class="nav-item"><a class="nav-link" href="contact.php">Contact</a></li>                         
            </ul>
            <ul class="navbar-nav navbar-mobile rt-nav">               
                <li class="nav-item">
                    <a class="nav-link hamburg" id="hamb-navicon"><span class="icon-Group-77 icomoon"></span></a>
                </li>				
            </ul>			
		</div>
		
	</header>
    <!--/ header -->
