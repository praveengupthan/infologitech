<?php
     //home slider 
     $homeSlider = array(
        array(
            "sl01", 
            "Delivery Expertise", 
            "Our delivery and support processes follow industry best practices. You can rest assured that not only will the project delivery be on time, but it will be executed using the most cost-optimized model for your requirements.",                        
        ),
        array(
            "sl02", 
            "Big Picture", 
            "We understand that this project is a part of your overall Big picture. With InfoLogitech services across the spectrum, we will committed to be engaged to partner with you for the successful implementation of any Platform.",                        
        ),
        array(
            "sl03", 
            "Customer Centricity", 
            "The Secret Sauce of our Success: Our existing client base remains highly satisfied, with 90% of our revenue coming from longstanding client engagements, many of which span several years. Partnerships require a commitment throughout the life of the engagement. InfoLogitech’s continued focus on customer centricity ensures our high standards and delivery of flawless services remain our top priority.",                        
        ),
        array(
            "sl04", 
            "Executive Commitment", 
            "Our customer centric governance model will ensure management focus through dedicated involvement of Senior Leadership.",                        
            )
    );

    //home services 
    $homeServices=array(
        array(
            "icon-enterprise-application", 
            "Enterprise Application Integration",
            "services.php#2"
        ),
        array(
            "icon-business-intelligence", 
            " Business Intelligence",
            "services.php#4"
        ),
        array(
            "icon-custom-application-development", 
            "Custom Application Development",
            "services.php#5"
        ),
        array(
            "icon-project-management", 
            " Projecet Management",
            "services.php#7"
        ),
    );

    //home testimonials
    $homeTestimonials=array(
        array(
            "Hari and his team have been an excellent partner to our team. They have provided several well qualified technical resources and management of their contractors has been a very positive experience. I would recommend Infologitech to other clients with contract resource needs.",
            "Daniel Brennan",
            "Echo Global"
        ),
        array(
            "
            Srini and team have provided Consultants with outstandinging expertise on Education domain and Technology who helped us build a path-breaking platform for Students and Universities . ",
            "Vamsi Vegunta",
            "Ubergrad"
        ),                              
    );

    //clients health
    $healthClients=array(
        array("health-amgen.jpg"),
        array("health-celgene.jpg"),
        array("health-cigna.jpg"),
        array("health-dentaquest.jpg"),
        array("health-nabp.jpg"),
        array("health-omnicare.jpg"),
        array("health-pfizer.jpg"),
        array("helth-sheridan.jpg"),
        array("health-stanley.jpg"),
        array("health-uhs.jpg"),
        array("health-valence.jpg"),
        array("health-walgreens.jpg")
    );
    //clients banking
    $bankingClients=array(
        array("banking-allianz.jpg"),
        array("banking-aviva.jpg"),
        array("banking-citigroup.jpg"),
        array("banking-hsa.jpg"),
        array("banking-itg.jpg"),
        array("banking-northern.jpg"),
        array("banking-pimco.jpg"),
        array("banking-pnc.jpg"),
        array("banking-statestreet.jpg"),
        array("banking-ubswar.jpg"),
        array("health-valence.jpg"),
        array("banking-wellsfargo.jpg")
    );
     //clients Manfacturing
     $ManufacturingClients=array(
        array("man-con.jpg"),
        array("man-corning.jpg"),
        array("man-first.jpg"),
        array("man-football.jpg"),
        array("man-ge.jpg"),
        array("man-goodyear.jpg"),
        array("man-lemans.jpg"),
        array("man-revere.jpg"),
        array("man-samsung.jpg"),
        array("man-tyson.jpg")       
    );
     //clients Manfacturing
     $insuranceClients=array(
        array("insurance-asi.jpg"),
        array("insurance-bsbs.jpg"),
        array("insurance-farmers.jpg"),
        array("insurance-homi.jpg"),
        array("insurance-humana.jpg"),
        array("insurance-nationwide.jpg"),
        array("insurance-penn.jpg"),
        array("insurance-principal.jpg"),
        array("insurance-providence.jpg"),
        array("insurance-we.jpg")       
    );
     //clients Software
     $softwareClients=array(
        array("soft-citrix.jpg"),
        array("soft-cts.jpg"),
        array("soft-dell.jpg"),
        array("soft-deloitte.jpg"),
        array("soft-microsoft.jpg"),
        array("soft-nuance.jpg"),
        array("soft-sumtotal.jpg"),       
    );

     //clients Software
     $networkingClients=array(
        array("net-at.jpg"),
        array("net-brightstar.jpg"),
        array("net-comcast.jpg"),
        array("net-cricket.jpg"),
        array("net-dish.jpg"),
        array("net-extreme.jpg"),
        array("net-siemens.jpg"),       
        array("net-tmobile.jpg"),    
    );
    //clients Software
    $transportClients=array(
        array("trans-american.jpg"),
        array("trans-echo.jpg"),
        array("trans-in.jpg"),
        array("trans-jetblue.jpg"),
        array("trans-sita.jpg"),
        array("trans-wmata.jpg")       
    );

     //clients Education
     $educationClients=array(
        array("edu-adesa.jpg"),
        array("edu-children.jpg"),
        array("edu-ets.jpg"),
        array("edu-first.jpg"),
        array("edu-lexisnexis.jpg"),       
    );
    //clients Media
    $mediaClients=array(
        array("media-walt.jpg"),        
    );
     //clients Media
     $federalClients=array(
        array("federal-new.jpg"),        
    );

    //service sub menu list 
    $serviceSubMenuItem=array(
        array(
            "2",
            "Enterprise Application Integration",
            "icon-enterprise"
        ),
        array(
            "3",
            "ETL",
            "icon-big-data"
        ),
        array(
            "4",
            "Business Intelligence Analytics & Big Data",
            "icon-data-1"
        ),
        array(
            "5",
            "Custom Application Development",
            "icon-custom-application-development"
        ),
        array(
            "6",
            "Quality Assurance",
            "icon-award"
        ),
        array(
            "7",
            "Project Management",
            "icon-project-management1"
        ),
        array(
            "8",
            "Enterprise Resource Planning",
            "icon-analytics"
        ),
        array(
            "9",
            "Content Management",
            "icon-file"
        ),
        array(
            "10",
            "Infrastructure Management",
            "icon-web-hosting"
        ),
    );

    //domains sub menu 
    $domainsSubmenuItems=array(
        array(
            "hCare",
            "Health Care",
            "icon-healthcare"
        ),
        array(
            "Networking",
            "Networking &amp; Communications",
            "icon-web-hosting"
        ),
        array(
            "Insurance",
            "Insurance",
            "icon-insurance"
        ),
        array(
            "Banking",
            "Banking & Financial",
            "icon-bank"
        ),
        array(
            "Logistics",
            "Logistics",
            "icon-boxes"
        ),
        array(
            "Education",
            "Education",
            "icon-graduated"
        ),
        array(
            "Manufacturing",
            "Manufacturing",
            "icon-power-plant"
        ),
        array(
            "Retail",
            "Retail",
            "icon-shopping-basket"
        )       
    );
?>