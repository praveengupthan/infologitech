<footer>
        <div class="container">
            <!-- row -->
            <div class="row pb-1 pb-sm-5 border-bottom">
                <!-- col -->
                <div class="col-md-3 col-sm-6">
                    <p>Infologitech is proud to be associated with Microsoft as most highly accredited independent technical support providers as part of Microsoft Partner network. We are Microsoft Gold certified Partner. <a href="about.php"> READ MORE..</a></p>

                    <ul class="footer-social border-top nav">
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://www.facebook.com/InfoLogitech.inc"><span class="icon-facebook"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://www.linkedin.com/checkpoint/challengesV2/AQG8Mhe7PyiP7AAAAXP3vDG5dym4Rr1xHbTGssQ0BGFyB7WtNOxP-2kBMlP5d6jdMzqx0MR80SGMmnT0YYLsjXywPo-STcuVeQ"><span class="icon-linkedin"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://twitter.com/InfoLogitechILT"><span class="icon-twitter"></span></a>
                        </li>
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-md-3 col-sm-6">
                    <h4 class="h5">Company</h4>
                    <ul class="footer-links">
                        <li><a href="index.php">Navigate your Home</a></li>
                        <li><a href="about.php">About Infologitech</a></li>                       
                        <li><a href="clients.php">Clients</a></li>
                        <li><a href="domains.php">Domains</a></li>
                        <li><a href="careers.php">Career</a></li>
                        <li><a href="contact.php">Contact us</a></li>
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-md-3  col-sm-6">
                    <h4 class="h5">Domains</h4>
                    <ul class="footer-links">
                        <li><a href="domains.php#hCare">Health Care</a></li>
                        <li><a href="domains.php#Networking">Networking &amp; Communications</a></li>
                        <li><a href="domains.php#Insurance">Insurance</a></li>
                        <li><a href="domains.php#Banking">Banking & Financial </a></li>
                        <li><a href="domains.php#Logistics">Logistics</a></li>
                        <li><a href="domains.php#Education">Education</a></li>
                        <li><a href="domains.php#Manufacturing">Manufacturing</a></li>
                        <li><a href="domains.php#Retail">Retail</a></li>
                    </ul>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-md-3  col-sm-6">
                    <h4 class="h5">Services</h4>

                    <ul class="footer-links">
                        <li><a href="services.php#2">Enterprise Application Integration</a></li>
                        <li><a href="services.php#3">ETL</a></li>
                        <li><a href="services.php#4">Business Intelligence Analytics & Big Data</a></li>
                        <li><a href="services.php#5">Custom Application Development</a></li>
                        <li><a href="services.php#6">Quality Assurance</a></li>
                        <li><a href="services.php#7">Project Management </a></li>
                        <li><a href="services.php#8">Enterprise Resource Planning</a></li>
                        <li><a href="services.php#9">Content Management</a></li>
                        <li><a href="services.php#10">Infrastructure Management</a></li>
                    </ul>
                </div>
                <!--/ col -->                
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row py-4">
                <div class="col-lg-12">
                    <p class="text-center mb-0 pb-0">Copyright © 2020 Infologitech</p>
                </div>
            </div>
            <!--/ row -->
        </div>
        <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>