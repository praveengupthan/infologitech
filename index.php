<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info Logitech</title>
     <?php
        include'includes/styles.php';
        include'includes/arrayObjects.php';
     ?>    
      <!--/ styles -->
</head>
<body class="homd-body">

    <?php
        include'includes/header.php'
    ?>
    <!-- main -->

    <div class="links">
        <a id="a1" data-id="a1" href="#1"><span>Navigate Home</span></a>
        <a id="a2" data-id="a2" href="#2"><span>Core Services</span></a>
        <a id="a3" data-id="a3" href="#3"><span>About</span></a>
        <a id="a4" data-id="a4" href="#4"><span>Products</span></a>       
        <a id="a5" data-id="a5" href="#5"><span>Testimonials</span></a>      
        <a id="a6" data-id="a6" href="#6"><span>Careers</span></a>      
    </div>

    <!-- section banner-->
    <section class="banner-home slidediv" id="1">       
        <!-- swiper -->     
        <div class="swiper-container home-slider">           
            <div class="swiper-wrapper">
                <?php
                    for($i=0;$i<count($homeSlider);$i++) { ?>
                <div class="swiper-slide <?php echo $homeSlider[$i][0]?>">
                    <!-- container -->
                    <div class="container">
                        <article>
                            <h1 class="h1 fwhite wow animate__animated animate__fadeInUp"> <?php echo $homeSlider[$i][1]?></h1>
                            <p class="wow animate__animated animate__fadeInUp animate__delay-1s"> <?php echo $homeSlider[$i][2]?></p>
                            <!-- <a href="javascript:void(0)" class="text-uppercase brd-link">Read More</a> -->
                        </article>      
                    </div>
                    <!--/ container -->
                </div>
                    <?php } ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
       <!--/ swiper -->
    </section>
    <!-- section banner -->
    <section class="services slidediv white-section" id="2">
        <!-- row -->
        <div class="row no-gutters">
            <!-- col -->
            <div class="col-md-6 d-none d-sm-block">
                <img src="img/services-leftimg.jpg" alt="" class="img-fluid serviceleft-img">
            </div>
            <!--/ col -->
            <!-- right col -->
            <div class="col-md-6">
                <div class="right-services text-left">                        
                    <article>
                        <h2 class="section-title fwhite w-100 pb-4 wow animate__animated animate__fadeInUp">Digital Core Services</h2> 
                        <!-- rpw -->
                        <div class="row">
                            <!-- col -->
                            <?php
                            for($i=0;$i<count($homeServices);$i++) { ?>
                            <div class="col-sm-6 col-6 service-col wow animate__animated animate__fadeInUp">
                               <span class="<?php echo $homeServices[$i][0]?> icomoon"></span>
                                <h4 class="h5 py-2">
                                <?php echo $homeServices[$i][1]?>
                                </h4>
                                <a class="fwhite link" href="<?php echo $homeServices[$i][2]?>">View More <span class="icon-long-arrow-right icomoon"></span> </a>
                            </div>
                                <?php } ?>
                            <!--/ col --> 
                        </div>
                        <!-- /row -->

                        <!-- row -->
                        <div class="row pt-md-1 wow animate__animated animate__fadeInUp">
                            <div class="col-lg-12">
                                <a class="brd-link textr-uppercase" href="services.php">More Services</a>
                            </div>
                        </div>
                        <!--/ row -->
                    </article>
                    
                </div>
                    
            </div>
            <!--/ right col -->
        </div>
        <!--/ row -->
    </section>
    <section class="aboutdiv slidediv" id="3">
        <!-- custom container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col-->
                <div class="col-sm-6 align-self-center">
                    <!-- article -->
                    <article class="ltarticle">
                        <p class="pb-0 mb-0">What We are</p>
                        <h2 class="section-title fwhite w-100 wow animate__animated animate__fadeInUp">About us</h2>
                        <P class="py-3 fwhite wow animate__animated animate__fadeInUp">INFOLOGITECH INC is a global IT development and services company specializing in delivering reliable, efficient, and profitable solutions to our clients through the four I's of business technology: Integration, Implementation, Infrastructure, and Innovation.</P>
                        <a class="brd-link textr-uppercase wow animate__animated animate__fadeInUp" href="about.php">Read More</a>
                    </article>
                    <!--/ articler -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-sm-6 align-self-center d-none d-sm-block">                      
                    <img src="img/about.svg" alt="" class="img-fluid wow animate__animated animate__fadeInUp">
                </div>
                <!--/ col -->                   
            </div>
            <!--/ row -->               
        </div>
        <!--/ custom container -->
    </section>

    <section class="products slidediv white-section" id="4">
        <!-- container -->
        <div class="cust-container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center order-md-last">
                  <article class="products-article">
                      <h2 class="section-title w-100 wow animate__animated animate__fadeInUp">Our Awesome Products</h2>
                      <P class="py-3 wow animate__animated animate__fadeInUp">We are currently working on Building Product catalogue by fostering Innovation within our Associates. The Products ideas are nurtured keeping in mind the future requirements of the Industries which build and engineer the future generations.</P>
                      <!-- <a class="brd-link textr-uppercase" href="javascript:void(0)">More Products</a> -->
                  </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-6 col-12">
                    <div class="product-row">
                        <div class="product-col d-none d-md-block"></div>
                        <div class="product-col wow animate__animated animate__fadeInUp">                            
                            <img src="img/data/products/product1.jpg" alt="" class="img-fluid">                           
                            <span class="title-product">Infowalk</span>
                        </div>
                        <div class="product-col wow animate__animated animate__fadeInUp">                          
                            <img src="img/data/products/product2.jpg" alt="" class="img-fluid">                           
                            <span class="title-product">Search - ED</span>
                        </div>
                    </div>
                    <div class="product-row">                       
                      <div class="product-col wow animate__animated animate__fadeInUp">                         
                         <img src="img/data/products/product3.jpg" alt="" class="img-fluid">                         
                          <span class="title-product">Signature Knowledge</span>
                      </div>
                      <div class="product-col wow animate__animated animate__fadeInUp">                         
                        <img src="img/data/products/product4.jpg" alt="" class="img-fluid">                         
                          <span class="title-product">Web Educator</span>
                      </div>
                      </div>
                </div>
                <!--/ col -->
                
            </div>
            <!--/ row -->
          </div>
          <!--/ container -->
    </section>
    <section class="testimonials slidediv pt-0" id="5">
       <!-- container -->
       <div class="cust-container">
            <!-- row -->
            <div class="row justify-content-center">
                <div class="col-lg-10 text-right">                        
                    <h2 class="section-title fwhite w-100">What Our People Says</h2>
                    <P class="fwhite">Every Infologicion is the navigator of our clients’ digital transformation</P> 
                </div>
                <!-- col -->
                <div class="col-lg-10">                  
                     <!-- Swiper -->
                        <div class="swiper-container test-div">
                            <div class="swiper-wrapper">
                                <?php
                                for($i=0;$i<count($homeTestimonials);$i++) { ?>
                                <div class="swiper-slide">                                       
                                    <p><?php echo $homeTestimonials[$i][0]?></p>                                  
                                    <div class="test-name">
                                        <h4 class="h5"><?php echo $homeTestimonials[$i][1]?></h4>
                                        <p class="text-right">
                                            <small><?php echo $homeTestimonials[$i][2]?></small>
                                        </p>
                                    </div>                                       
                                </div>      
                                <?php } ?>                       
                            
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <!-- <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div> -->
                        </div>
                    </div>
                <!--/ col -->
            </div>
            <!-- row -->
        </div>
        <!--/ container -->
        <!-- Swiper -->

    </section>
    <section class="careers slidediv white-section pt-4" id="6">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <div class="col-lg-10 text-center">
                    <div class="">
                        <h2 class="section-title wow animate__animated animate__fadeInUp">Careers</h2>
                        <P class="wow animate__animated animate__fadeInUp">Every Infologician is the navigator of our clients’ 
                            digital transformation
                         </P> 
                    </div>
                    <img src="img/office.jpg" alt="" class="img-fluid wow animate__animated animate__fadeInUp">
                </div>
                <div class="col-lg-8 text-center careertext-col wow animate__animated animate__fadeInUp">
                    <h4 class="h4 p-4 wow animate__animated animate__fadeInUp">Find opportunities right for you</h4>
                    <a href="careers.php" class="brd-link wow animate__animated animate__fadeInUp">Explore Careers</a>
                </div>
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </section>
    <!-- main-->

    <?php
        include'includes/footer.php'
    ?>

    <?php
        include'includes/scripts.php'
    ?>    
   
</body>
</html>